const mongoose = require('mongoose');
const { mongo, env } = require('./vars');
const EventType = require('./../api/models/eventType.model');
const EventModel = require('./../api/models/event.model');

const { eventTypeInitData, eventsInitData } = require('./db_init');

// Exit application on error
mongoose.connection.on('error', (err) => {
  console.error(`MongoDB connection error: ${err}`);
  process.exit(-1);
});

// print mongoose logs in dev env
if (env === 'development') {
  mongoose.set('debug', true);
}

/**
* Connect to mongo db
*
* @returns {object} Mongoose connection
* @public
*/
exports.connect = () => {
  mongoose.Promise = global.Promise;
  mongoose.connect(mongo.uri, {
    keepAlive: 1,
  }).then(
    async () => {
      /* await EventType.remove({});
      eventTypeInitData.forEach(async (eventTypeData) => {
        const eventType = new EventType(eventTypeData);
        eventType.save();
      }); */
      try {
        await EventModel.remove({});
        eventsInitData.forEach(async (eventInitData) => {
          const event = new EventModel(eventInitData);
          console.log(event);
          event.save().catch(e => console.log(e));
        });
      } catch (e) {
        console.log(e);
      }
    },
    (err) => { console.log(err); },
  );
  return mongoose.connection;
};
