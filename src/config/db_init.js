const eventTypeInitData = [
  {
    title: 'bag-snatching',
    description: 'bag-snatching-desc',
    icon: 'bag-snatching.png',
  },
  {
    title: 'groping',
    description: 'groping-desc',
    icon: 'groping.png',
  },
];

const eventsInitData = [{
  eventType: '5a8fc8c4e8dde545aa5f3785',
  extraData: 'This is kind of comment user can do I',
  userId: '5a9368d778497550aee053a6',
  loc: {
    type: 'Point',
    coordinates: [139.737151, 35.6654717],
  },
}];

module.exports = {
  eventTypeInitData,
  eventsInitData,
};
