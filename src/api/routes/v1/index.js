const express = require('express');
const userRoutes = require('./user.route');
const eventTypesRoutes = require('./eventType.route');
const eventRoutes = require('./event.route');
const authRoutes = require('./auth.route');

const router = express.Router();
/**
 * GET v1/status
 */
router.get('/status', (req, res) => res.send('OK'));

/**
 * GET v1/docs
 */
// router.use('/docs', express.static('docs'));
router.use('/users', userRoutes);
router.use('/events', eventRoutes);
router.use('/event-types', eventTypesRoutes);
router.use('/auth', authRoutes);

module.exports = router;
