const mongoose = require('mongoose');

const DEFAULT_DISTANCE = 3 * 1000;

const { ObjectId } = mongoose.Schema.Types;
/**
 * Event Schema
 * @private
 */


const eventSchema = new mongoose.Schema({

  eventType: { type: ObjectId, required: true },
  extraData: { type: String, maxlength: 500, trim: true },
  userId: { type: ObjectId, required: true },
  created: { type: Date, default: Date.now, required: true },
  updated: { type: Date, default: Date.now, required: true },
  loc: {
    type: { type: String },
    coordinates: [],
  },
});

eventSchema.index({ loc: '2dsphere' });

eventSchema.pre('update', async function save(next) {
  try {
    this.updated = Date.now();
    return next();
  } catch (error) {
    return next(error);
  }
});


eventSchema.statics = {
  /**
   * List users in descending order of 'createdAt' timestamp.
   *
   * @param {number} skip - Number of users to be skipped.
   * @param {number} limit - Limit number of users to be returned.
   * @returns {Promise<User[]>}
   */
  list({
    page = 1,
    perPage = 30,
    type,
    userId,
    lat,
    lon,
    dist,
  }) {
    const options = {};
    if (lat && lon) {
      const maxDistance = (dist) ? dist * 1000 : DEFAULT_DISTANCE;
      options.loc = {
        $near: {
          $geometry: {
            type: 'Point',
            coordinates: [lon, lat],
          },
          $maxDistance: maxDistance,
        },
      };
    }
    if (type && type.length !== 0) options.eventType = type;
    if (userId) options.userId = userId;
    return this.find(options)
      .sort({ createdAt: -1 })
      .skip(perPage * (page - 1))
      .limit(perPage)
      .exec();
  },
};

module.exports = mongoose.model('Event', eventSchema);
