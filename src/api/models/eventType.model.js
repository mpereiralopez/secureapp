const mongoose = require('mongoose');
const baseIcons = require('./../../../static/images/base64');

const fieldsToTranslate = ['title', 'description'];

/**
 * EventType Schema
 * @private
 */


const eventTypeSchema = new mongoose.Schema({
  title: { type: String, maxlength: 50, trim: true },
  description: { type: String, maxlength: 500, trim: true },
  icon: { type: String, required: true, trim: true },
  created: { type: Date, default: Date.now, required: true },
  updated: { type: Date, default: Date.now, required: true },
});

eventTypeSchema.virtual('iconData');

eventTypeSchema.pre('save', async function save(next) {
  try {
    this.updated = Date.now();
    return next();
  } catch (error) {
    return next(error);
  }
});

/**
 * Methods
 */
eventTypeSchema.method({
  transform(params) {
    const { lang, def } = params;
    const transformed = this.toJSON();
    fieldsToTranslate.forEach((field) => {
      // find if the value in field have a translation in lang
      let translatedValue = lang[Object.keys(lang).find(key => key === transformed[field])];
      if (!translatedValue) {
        translatedValue = lang[Object.keys(def).find(key => key === transformed[field])];
      }
      transformed[field] = translatedValue;
    });

    const base64Image = Object.keys(baseIcons).find(icon => icon === transformed.icon);
    console.log('base64Image');
    console.log(base64Image);
    transformed.iconData = {
      data: baseIcons[base64Image],
      contentType: 'image/png',
    };
    delete transformed.icon;
    return transformed;
  },
});


eventTypeSchema.statics = {
  /**
   * List users in descending order of 'createdAt' timestamp.
   *
   * @param {number} skip - Number of users to be skipped.
   * @param {number} limit - Limit number of users to be returned.
   * @returns {Promise<User[]>}
   */
  list({ page = 1, perPage = 30 }) {
    // const options = { name, email, role };

    return this.find({})
      .sort({ createdAt: -1 })
      .skip(perPage * (page - 1))
      .limit(perPage)
      .exec();
  },
};


module.exports = mongoose.model('EventType', eventTypeSchema);
