
const Event = require('../models/event.model');

/**
 * Get eventType list
 * @public
 */
exports.list = async (req, res, next) => {
  try {
    const events = await Event.list(req.query);
    const eventsTransformed = events.map((event) => {
      const eventJSON = event.toJSON();
      eventJSON.latitude = eventJSON.loc.coordinates[1];
      eventJSON.longitude = eventJSON.loc.coordinates[0];
      delete eventJSON.deviceId;
      delete eventJSON.__v;
      delete eventJSON.loc;
      return eventJSON;
    });
    res.json(eventsTransformed);
  } catch (error) {
    next(error);
  }
};
