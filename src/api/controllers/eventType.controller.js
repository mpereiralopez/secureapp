
const EventType = require('../models/eventType.model');

const { defaultLocale } = require('./../../config/vars');
const avalaibleLocales = require('./../../../static/i18n');
/**
 * Get eventType list
 * @public
 */
exports.list = async (req, res, next) => {
  try {
    const locale = req.get('Accept-Language') || defaultLocale;
    const dictionary = {
      lang: avalaibleLocales[locale] || avalaibleLocales[defaultLocale],
      def: avalaibleLocales[defaultLocale],
    };
    const eventTypes = await EventType.list(req.query);
    const attacheIcons = eventTypes.map(event => event.transform({ ...dictionary }));
    res.setHeader('Content-Type', 'application/json');
    res.json(attacheIcons);
  } catch (error) {
    next(error);
  }
};
