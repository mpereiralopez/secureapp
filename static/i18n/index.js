const fs = require('fs');
const path = require('path');

const normalizedPath = path.join(__dirname, '');

fs.readdirSync(normalizedPath).forEach((file) => {
  if (file !== 'index.js') {
    const name = file.replace('.json', '');
    exports[name] = require(`${normalizedPath}/${file}`);
  }
});
